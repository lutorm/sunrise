#include "preferences.h"
#include "model.h"
#include <string>
#include <fstream>

using namespace std;
using namespace mcrx;

/* Test that the mappings models output sensible data for the
   different parameter sets. */

int main(int, char**)
{
  Preferences p;
  p.setValue("mappings_mcl",1e5);
  p.setValue("mappings_pdr_fraction",0.0);
  p.setValue("mappings_mcl",1e5);
  p.setValue("min_wavelength",1e-7);
  p.setValue("max_wavelength",1e-3);
  p.setValue("mappings_mcl",1e5);
  p.setValue("mappings_sed_file",string("~/mappings/Smodel.fits"));

  mappings_stellarmodel mpdr0(p);    
  p.setValue("mappings_pdr_fraction",1.0);
  mappings_stellarmodel mpdr1(p);    

  ofstream of("mappings_test.out");

  array_1 z(5), Pk(5), S(6);
  z = .05, .2 , .4, 2;
  z*=0.02;
  Pk = 4,5,6,7,8;
  S=4,4.5,5,5.5,6,6.5;

  for(int zz=0; zz<z.size(); ++zz)
    for(int ppk=0; ppk<Pk.size(); ++ppk)
      for(int ss=0; ss<S.size(); ++ss)
	{
	  array_1 sed0 = mpdr0.get_SED(1e6, z(zz), pow(10,Pk(ppk)), S(ss));
	  array_1 sed1 = mpdr1.get_SED(1e6, z(zz), pow(10,Pk(ppk)), S(ss));
	  for(int i=0; i<sed0.size(); ++i)
	    of << mpdr0.lambda()(i) << '\t' << sed0(i) << '\t' << sed1(i) << '\n';
	  of << "\n\n";
	}
  
}
