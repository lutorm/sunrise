#include <UnitTest++.h>
#include "config.h"
#include <iostream>

class hstate {
public:
  uint8_t data_;


  hstate() { write_data(0, 1, 0, 0, 0); };

  /** Assembles the data byte from the specified dimensions and axes. */
  void write_data(uint8_t dim1, uint8_t dim2, 
		  uint8_t a1, uint8_t a2, uint8_t a3) {
    data_ = dim1 | (dim2<<'\x02') |
      (a1<<'\x04') | (a2<<'\x05') | (a3<<'\x06'); };
};

std::ostream&
operator<<(std::ostream& os, const hstate& s)
{
  os << s.data_;
  return os;
}

class statevector {
public:
  hstate state_[21];
  uint8_t n_;

  statevector() : n_(0) {};
};

TEST(alignment_error1)
{
  statevector s;
  std::cout << s.state_[0] << std::endl;
}

TEST(alignment_error2)
{
  char c;
  statevector s;
  statevector s1;
  c=s.state_[1].data_;
  std::cout << c << s.state_[0] << std::endl;
  std::cout << c << s1.state_[0] << std::endl;
}
