/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Explicit instantiations of the xfer classes, since we prevent
/// instantiations of them in the stage classes. The other xfer
/// templates (including for the Arepo grid) are instantiated in
/// xfer_inst2.cc to give better build parallelism. \ingroup mcrx

#include "mcrx.h"
#include "xfer_impl.h"
#include "grid.h"
#include "full_sed_grid.h"
#include "aux_grid.h"
#include "dust_model.h"
#include "scatterer.h"
#include "dummy.h"
#include "mpi_master_impl.h"

// full instantiation of dust_model, full_sed_grid<adaptive_grid>
typedef 
mcrx::xfer<mcrx::T_polychromatic_dust_model, mcrx::T_full_sed_adaptive_grid> T1;
// can't explicitly instantiate a typedef...
template class
mcrx::xfer<mcrx::T_polychromatic_dust_model, mcrx::T_full_sed_adaptive_grid>;

template void mcrx::mpi_master<T1>::thread_send_ray(const typename T1::T_queue_item&, int, int);

// also instantiate an xfer object for a grid with only an absorber,
// which is used by some of the tests.
template class
mcrx::xfer<mcrx::T_polychromatic_dust_model, mcrx::full_sed_grid<mcrx::adaptive_grid<mcrx::absorber<mcrx::array_1> > > >;

// this propagate is used by mlt_xfer
template bool T1::propagate<true, false, false, false>(double);

template void mcrx::mpi_master<mcrx::xfer<mcrx::T_polychromatic_dust_model, mcrx::full_sed_grid<mcrx::adaptive_grid<mcrx::absorber<mcrx::array_1> > > > >::thread_send_ray(const typename mcrx::xfer<mcrx::T_polychromatic_dust_model, mcrx::full_sed_grid<mcrx::adaptive_grid<mcrx::absorber<mcrx::array_1> > > >::T_queue_item&, int, int);


// for these versions, we don't instantiate the entire class because
// the dummy_grid does not define the required operations for doing
// scattering. The required methods are constructor, destructor, and
// shoot_isotropic(), rng(), uses_local_emergence().

// real dust model, dummy grid

/*
template class
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid>;

template
mcrx::xfer<T_polychromatic_dust_model, mcrx::dummy_grid, mcrx::mcrx_rng_policy>::
xfer(T_grid&, T_emission&, T_emergence&, const T_dust_model&, T_rng_policy&, const T_dust_model::T_biaser&, T_float, T_float, T_float, T_float, bool, bool, bool, int,int);
template
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::xfer(const xfer&);
template
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::~xfer();

template void 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::shoot_isotropic();
template mcrx::mcrx_rng_policy&
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::rng();
template bool
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::uses_local_emergence() const;
*/
/*
template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::shoot_isotropic()

template class mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
			   mcrx::dummy_grid, 
			   mcrx::mcrx_rng_policy>;

template class mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
			   mcrx::aux_grid<mcrx::adaptive_grid,
					  mcrx::cumulative_sampling, 
					  mcrx::local_random>,
			   mcrx::mcrx_rng_policy>;
*/

// generic dust model, dummy grid
/*
template
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::xfer(T_grid&, T_emission&, T_emergence&, const T_dust_model&, T_rng_policy&, const T_dust_model::T_biaser&, T_float, T_float, T_float, T_float, bool, bool, bool, int,int);
template
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::xfer(const xfer&);
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::~xfer();
template void 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::shoot_isotropic();
template mcrx::mcrx_rng_policy&
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::rng();
template bool
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::dummy_grid, 
	   mcrx::mcrx_rng_policy>::uses_local_emergence() const;

*/
