/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file 

    Declaration of the density_generator classes which are responsible
    for converting GADGET particle data into vectors of scatterer
    densities.  */

// $Id$

#ifndef __density_generator__
#define __density_generator__


#include "mcrx-units.h"
#include "ray.h" // for T_densities
#include "multiphase.h"

namespace mcrx {
  class density_generator;
  class uniform_density_generator;
}

// forward decl
class Preferences;

/** The abstract base class for the density_generator classes.  */
class mcrx::density_generator {
public:
  /** Returns the number of species, i.e. the length of the density
      vector returned by the generator.  */
  virtual int n_species() const = 0;

  /** This function is responsible for generating a density vector of
      different scatterers out of the densities of gas and metals.  */
  virtual T_densities make_density_vector (T_float, T_float) const=0;
};

/** Density_generator class for a simple uniform dust model with one
    kind of scatterer.  */
class mcrx::uniform_density_generator: public density_generator {
private:
  /// Dust to gas ratio.
  T_float dtg_;

  /// Dust to metal ratio.
  T_float mtg_;

  /// multiphase model 
  bool use_mp_;
  multiphase mp_;

public:
  uniform_density_generator (T_float, T_float);
  uniform_density_generator (Preferences& p);

  virtual int n_species() const {return 1;};
  virtual T_densities make_density_vector (T_float, T_float) const;
};

#endif

  
