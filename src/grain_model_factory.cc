/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file 

    Contains static initialization of the grain model factory. All
    grain models must put their initialization routines here, and this
    file must be compiled directly into the executable.  It cannot be
    put into a static library, because then the static initialization
    will not work. */

#include "wd01_grain_model.h"
#include "wd01_Brent_PAH_grain_model.h"
#include "p04_grain_model.h"
#include "dl07_template_grain_model.h"
#include "baes_grain_model.h"
#include "blackbody_grain.h"

namespace {
  /** Registers the wd01_grain_model creator function with the
      GrainModelFactory. */
  bool wd01_grain_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("wd01",mcrx::wd01_grain_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

namespace {
  /** Registers the wd01_Brent_model creator function with the
      GrainModelFactory.  */
  bool wd01_Brent_PAH_model_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("wd01_Brent_PAH",mcrx::wd01_Brent_PAH_model_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

namespace {
  /** Registers the p04_model creator function with the
      GrainModelFactory.  */
  bool p04_model_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("p04",mcrx::p04_grain_model_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

namespace {
  /** Registers the dl07_template_grain_model creator function with the
      GrainModelFactory.  */
  bool dl07_template_grain_model_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("dl07_template",mcrx::dl07_template_grain_model_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

namespace {
  /** Registers the baes_grain_model creator function with the
      GrainModelFactory.  */
  bool baes_grain_model_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("baes",mcrx::baes_grain_model_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

namespace {
  /** Registers the blackbody_grain_model creator function with the
      GrainModelFactory.  */
  bool blackbody_grain_model_creator_functor_good=mcrx::GrainModelFactory::Instance().Register("blackbody",mcrx::blackbody_grain_model_creator_functor<mcrx::polychromatic_scatterer_policy,mcrx::mcrx_rng_policy>());
};

