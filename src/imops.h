/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file

    Declarations of functions to perform generic operations on the
    image arrays. */

// $Id$

#ifndef __imops__
#define __imops__

#include "terminator.h"
#include "mcrx-units.h"
#include "mcrx-types.h"
#include "boost/function.hpp"
#include <string>

namespace CCfits {
  class FITS;
};

namespace mcrx {

/** This function integrates an image to obtain the integrated
    spectrum, and writes it to the specified column in the
    INTEGRATED_QUANTITIES. The conversion to luminosity is made using
    the "cameradist" keyword in the camera parameters HDU.

    Since the images are surface brightness, they are converted to
    (inferred) luminosity by multiplying by the solid angle subtended
    by the pixels (found in the -PARAMETETRS image) and then
    multiplying by 4*pi*d^2, where d is the camera distance in m,
    since these are the units in the image. After this, the units are
    L_lambda units.
 */
mcrx::array_1 
integrate_image(CCfits::FITS& file, 
		/// Name of HDU that contains the image.
		const std::string& hdu_name,
		/// Name of HDU that contains the camera parameters
		const std::string& param_hdu_name,
		/// Name of the output column. If this is an empty
		/// string, it is not written.
		const std::string& column_name,
		/// Wavelength array (for integrating bolometric lum).
		const array_1& wavelengths,
		/// Name of keyword to which bolometric luminosity is
		/// writen. If this is an empty string, it is not
		/// written.
		const std::string& bol_keyword,
		/// Unit map where luminosity and L_lambda units are obtained.
		const T_unit_map& units,
		/// Function which returns true if the calculation
		/// should be interrupted.
		boost::function<terminator::t_type()> term);
}

#endif
