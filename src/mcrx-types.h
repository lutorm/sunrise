/*
    Copyright 2006-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Declares types to be used with the mcrx classes.

#ifndef __mcrx_types__
#define __mcrx_types__

namespace blitz {
  template <typename, int> class Array;
  template <typename, int> class TinyVector;
}

namespace mcrx {
  /// Floating-point type used.  We use the typedef so it can be
  /// easily changed.
  typedef double T_float;
  /// Floating-point 3-D vector type.
  typedef blitz::TinyVector <T_float, 3> vec3d;
  /// Integer 3-D vector type.
  typedef blitz::TinyVector <int, 3> vec3i;

  /// Floating-point 2-D vector type.
  typedef blitz::TinyVector <T_float, 2> vec2d;
  /// Integer 2-D vector type.
  typedef blitz::TinyVector <int, 2> vec2i;

  /// One-dimensional blitz array.
  typedef blitz::Array <T_float, 1> array_1;
  /// Two-dimensional blitz array.
  typedef blitz::Array <T_float, 2> array_2;
  /// Three-dimensional blitz array.
  typedef blitz::Array <T_float, 3> array_3;
  typedef blitz::Array <T_float, 4> array_4;

  /// Densities of dust is a 1d array.
  typedef array_1 T_densities;
  /// Gradient of densities is a 1d multicomponent array of TinyVectors.
  typedef blitz::Array<vec3d, 1> T_density_gradients;

  /// The rng policy we normally use.
  class local_random;
  typedef local_random mcrx_rng_policy;
}

#endif
