/*
    Copyright 2007-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Contains the execution functions of the mcrx stages. \ingroup mcrx

#include "config.h"
#include "mcrx-stage.h"
#include "arepo_grid.h"
#include "arepo_emission.h"


void mcrx::Mcrx::execute_adaptive_stages()
{  
  // Each stage *MUST* be in separate scope, because otherwise we can
  // not ensure that the fits file has been closed, which will cause
  // open as read/write to fail.
  {
    nonscatter_stage a(*this);
    a();
  }
  {
    scatter_stage<adaptive_grid> a(*this);
    a();
  }
  /*
  {
    intensity_stage<adaptive_grid> a(*this);
    a();
  }
  */
  {
    aux_grid_stage<adaptive_grid> a(*this);
    a();
  }
  {
    aux_particle_stage a(*this);
    a();
  }
  
  if(p.defined("ir_equilibrium_tolerance")) {
    // use dust self absorption
    {
      ir_intensity_stage<adaptive_grid> a(*this);
      a();
    }
    {
      thick_ir_stage<adaptive_grid> b(*this);
      b();
    }
  }
  else {
    // if equilibrium tolerance is not specified, assume it's 1 and
    // use the optically thin dust emission approximation

    /// \todo if we want to keep this, we need to execute the intensity stage
    ir_stage<adaptive_grid> a(*this);
    a();
  }
}


#ifdef WITH_AREPO

void mcrx::Mcrx::execute_arepo_stages()
{
  {
    nonscatter_stage a(*this);
    a();
  }
  {
    scatter_stage<arepo_grid> a(*this);
    a();
  }
  /*
  {
    intensity_stage<arepo_grid> a(*this);
    a();
  }
  */
  {
    aux_grid_stage<arepo_grid> a(*this);
    a();
  }
  {
    aux_particle_stage a(*this);
    a();
  }
  
  if(p.defined("ir_equilibrium_tolerance")) {
    // use dust self absorption
    {
      ir_intensity_stage<arepo_grid> a(*this);
      a();
    }
    {
      thick_ir_stage<arepo_grid> b(*this);
      b();
    }
  }
  else {
    // if equilibrium tolerance is not specified, assume it's 1 and
    // use the optically thin dust emission approximation

    /// \todo if we want to keep this, we need to execute the intensity stage
    ir_stage<arepo_grid> a(*this);
    a();
  }
}

#endif
