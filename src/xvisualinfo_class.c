/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file xvisualinfo_class.c Contains the getXVisualInfo_class
    function. */

#ifdef WITH_X11

#include <X11/Xutil.h>

/** This C function is necessary because class is a reserved keyword
    in C++ so we can only retrieve it in C. It just returns the class
    member of the supplied XVisualInfo struct. */
int* getXVisualInfo_class(XVisualInfo* xv)
{
  return &(xv->class);
}

#endif

