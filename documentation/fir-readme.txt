Fir is the program that calculates images of bolometric dust emission
based on the energy deposition data calculated by mcrx.  It takes the
mcrx output file and produces a file containing dust emission images
of the object for all the cameras.

Fir takes a configuration filename as its only argument.  The
configuration file consists of keyword-value pairs which are used to
control the behavior of the program.

The required keywords are:
input_file	mcrx_000.fits
		The input file (created by mcrx).
output_file	fir_000.fits
		The name of the output FITS file where the results are
		saved.
nrays		1000000
		The number of rays to be shot.
seed		1
		The random number seed.		

Optional keywords are:
n_threads	16
		The number of threads over which the shooting
		will be distributed.  The default value is 1.
use_counters	false
		Specifies whether counters should print to standard
		out.  This is nice if you are running interactively,
		but makes log files messy for batch jobs.  Default is
		true. 
use_hpm		true
		Specifies whether the HPM Toolkit should be used for
		performance analysis.  Default is false.
CCfits_verbose	true
		Specifies whether CCfits should be in verbose mode,
		which will print diagnostic messages if there's a
		problem.  However, it will also print messages that
		look suspicious but are normal, so the default is
		false.

The output file is a FITS file.  In order to propagate all the
necessary input information, it contains a copy of all the HDU's in
the input grid file except the "CAMERAi",
"CAMERAi-SCATTER/NONSCATTER/ATTENUATION", "GRIDSTRUCTURE", "LAMBDA",
"DEPOSITION","RANDOM_STATE" and "INTEGRATED_QUANTITIES" HDU's.  The
pHDU has the following keywords set:

FILETYPE    "FIR" 
	    The file contains infrared dust emission images.
DATATYPE    "GRID"
	    The data is on a grid (as opposed to a particle set).
FIRCODE     "FIR"
	    Specifies the code which was used to calculate the
	    infrared images, as well as the HDU in which the
	    configuration keywords can be found.
	    Currently, only FIR is used here.

HDU "FIR" (or whatever is specified by the FIRCODE keyword):
Contains the input parameters from the configuration file.
   					
HDU's "CAMERAi-FIR":
These HDU's contain images of the bolometric dust emission for the
appropriate camera, calculated from the data in the DEPOSITION HDU
output by mcrx.  They also contain the following keywords:
SB_FACTR   double  Conversion factor from internal units to surface brightness
IMUNIT	   string  The unit of the pixel quantities
PIXEL_SR   double  The solid angle subtended by the pixels in the
		   image.
normalization1	   double	Image normalization (internal usage).

Note that because of a bug in mcrx or CCfits, these HDU's are actually
data cubes with a third dimension of 2, and one of the slices are
empty.  (This should be fixed...)
